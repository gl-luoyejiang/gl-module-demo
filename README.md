
<!-- TOC -->

- [GL.iNet Router API Development Guide](#glinet-router-api-development-guide)
    - [API Encapsulation](#api-encapsulation)
    - [API Entity](#api-entity)
    - [API Document](#api-document)
    - [API Develop](#api-develop)
    - [API Compile](#api-compile)
    - [API Install](#api-install)
    - [API Debug](#api-debug)

<!-- /TOC -->

- [中文版](README_cn.md)

# GL.iNet Router API Development Guide

This document will introduce the GL.iNet router API development guide, from which you can learn how to write and debug API programs.

## API Encapsulation

```
/* gllibbase.h */

typedef struct _api_info{
	/* the string for the function */	
	const char* Path;
	/* the allowed operations of "get" or "post" */
	const char* AllowedOperations;
	/* the handle of the target function */
	api_call TargetFunctionHandler;
} api_info_t;

#define map(path, opts, handler) { \
	.Path=path, \
	.AllowedOperations=opts, \
	.TargetFunctionHandler=handler \
}
```

You need to define an API array variable based on the API's encapsulation. Please refer to the test demo for details.

## API Entity

```
extern api_info_t* get_api_entity(int* pLen);
```

You need to implement this function definition and expose the API entry to the API parser. Please refer to the test demo for details.

## API Document

https://dev.gl-inet.com/router-api/2/，Use this API documentation to upgrade your firmware to version 3.025.

## API Develop

Please refer to the demo program for development. Can be added and modified directly on the basis of the demo program.

## API Compile

The program can be compiled using OpenWrt source code, or it can be compiled using the OpenWrt SDK, which compiles faster.

- OpenWrt Source Code：https://github.com/gl-inet/openwrt

1. Download OpenWrt source code，`git clone https://github.com/gl-inet/openwrt.git openwrt`.

2. According to https://github.com/gl-inet/openwrt/blob/develop/README.md to build compile the environment.

3. Put the demo program in the openwrt/package directory，execute compile command

   `make package/gl-api-demo/compile`，ipk package will be generated in the bin/ directory.

- OpenWrt SDK：https://github.com/gl-inet/sdk

1. Download OpenWrt SDK，`git clone https://github.com/gl-inet/sdk.git sdk.`

2. According to https://github.com/gl-inet/sdk/blob/master/README.md to build compile the environment.

3. Put the demo program in the sdk/\<target>/package directory，execute compile command

   `cd sdk/<target>/`

   `make package/gl-api-demo/compile`，

   ipk package will be generated in the bin/ directory.

## API Install

Use the WinSCP tool to upload the ipk to the router /tmp/ directory for installation.，WinSCP download url is https://winscp.net/eng/download.php，installation and uninstallation commands are as follows：

- Installation：opkg install gl-api-demo_3.0.0-1_mips_24kc.ipk
- Uninstallation ：opkg remove gl-api-demo

## API Debug

- curl

1. get token

```
# curl -X POST 192.168.8.1/cgi-bin/api/router/login -d "pwd=goodlife1"
{"code":0,"token":"ad959856a9894adcbcb07ade1eda63bf"}
```

2. /demo/hello

```
# curl -X GET -H "Authorization: ad959856a9894adcbcb07ade1eda63bf" 192.168.8.1/cgi-bin/api/demo/hello
{"code":0,"msg":"Hello GL.iNet API"}
```

3. /demo/hostname_get

```
# curl -X GET -H "Authorization: ad959856a9894adcbcb07ade1eda63bf" 192.168.8.1/cgi-bin/api/demo/hostname_get
{"code":0,"hostname":"GL-USB150"}
```

4. /demo/hostname_set

```
# curl -X POST -H "Authorization: ad959856a9894adcbcb07ade1eda63bf" 192.168.8.1/cgi-bin/api/demo/hostname_set -d "hostname=GL-S1300"
{"code":0}
```

- Postman

1. get token

> Headers parameter

Content-Type：application/x-www-form-urlencoded

> Body parameter

Encoding format：x-www-form-urlencoded

KEY：pwd，VALUE：goodlife1

> response

```
{
    "code": 0,
    "token": "89ae4e1f76064ba38fa844aaaaa70b3a"
}
```

2. /demo/hello

> Headers parameter

Content-Type：application/x-www-form-urlencoded

Authorization：89ae4e1f76064ba38fa844aaaaa70b3a

> Body parameter

Encoding format：x-www-form-urlencoded

> response

```
{
    "code": 0,
    "msg": "Hello GL.iNet API"
}
```

3. /demo/hostname_get

> Headers parameter

Content-Type：application/x-www-form-urlencoded

Authorization：89ae4e1f76064ba38fa844aaaaa70b3a

> Body parameter

Encoding format：x-www-form-urlencoded

> response

```
{
    "code": 0,
    "hostname": "GL-USB150"
}
```

4. /demo/hostname_set

> Headers parameter

Content-Type：application/x-www-form-urlencoded

Authorization：89ae4e1f76064ba38fa844aaaaa70b3a

> Body parameter

Encoding format：x-www-form-urlencoded

KEY：hostname，VALUE：test

> response

```
{
    "code": 0
}
```

