
#include <uci.h>
#include "demo.h"


int demo_hello(json_object* input, json_object* output)
{
	json_object_object_add(output, "msg", json_object_new_string("Hello GL.iNet API"));
	return 0;
}

int demo_hostname_get(json_object* input, json_object* output)
{
	char value[128] = {0};
	struct uci_context* ctx = uci_alloc_context();
	struct uci_ptr ptr;
	struct uci_element *e;

	char *sec = strdup("system.@system[0].hostname");
	if (uci_lookup_ptr(ctx, &ptr, sec, true) == UCI_OK) {
		//json_object_object_add(output, "hostname", json_object_new_string("Hello World"));

		e = ptr.last;
		switch(e->type) {
		case UCI_TYPE_SECTION:
			sprintf(value, "%s", ptr.s->type);
			break;
		case UCI_TYPE_OPTION:
			switch(ptr.o->type) {
			case UCI_TYPE_STRING:
				sprintf(value, "%s", ptr.o->v.string);
				break;
			case UCI_TYPE_LIST:
				//TODO
				break;
			default:
				strcpy(value, "");
				break;
			}

			break;
		default:
			break;
		}
	}

	uci_free_context(ctx);
	
	json_object_object_add(output, "hostname", json_object_new_string(value));

	return 0;
}

int demo_hostname_set(json_object* input, json_object* output)
{
	json_object *o = NULL;
	if (!json_object_object_get_ex(input, "hostname", &o)) {
		printf("Filed does not exist\n");
		return -5; 
	}

	const char *hostname =  json_object_get_string(o);
	
	struct uci_context* ctx = uci_alloc_context();
	struct uci_ptr ptr;
	//char *sec = strdup("system.@system[0].hostname");
	char str[128] = {0};
	sprintf(str, "system.@system[0].hostname=%s", hostname);
	if (uci_lookup_ptr(ctx, &ptr, str, true) == UCI_OK) {
		if (uci_set(ctx, &ptr) == UCI_OK) {
			uci_save(ctx, ptr.p);
		}

		uci_commit(ctx, &ptr.p, false);
	}

	uci_free_context(ctx);

	return 0;
}

/** The implementation of the GetAPIFunctions function **/
#include "glapibase.h"

static api_info_t gl_lstCgiApiFuctionInfo[] = {
		map("/demo/hello", "get", demo_hello),
		map("/demo/hostname_get", "get", demo_hostname_get),
		map("/demo/hostname_set", "post", demo_hostname_set),
};

api_info_t* get_api_entity(int* pLen)
{
	(*pLen) = sizeof(gl_lstCgiApiFuctionInfo) / sizeof(gl_lstCgiApiFuctionInfo[0]);
	return gl_lstCgiApiFuctionInfo;
}

